# p2p-sourcenet

Attempt at constructing a p2p system for git.

See: https://www.ctrl.blog/entry/git-p2p-compared.html for info on current options.

## Goals
* Create a git transport and associated daemon to enable decentralised, author-as-crypto-signature distribution of git repositories.
* Allow for easy use of transports over the Tor Network (and potentially I2P) for anonymity and anti-censorship aspects
* Enable the use of smart git-packfiles and normal mutability rather than current awkwardness

## Specific targets:
* Should be able to specify git remotes with a URL format that embeds the cryptographic identity of the author(s) with some embedded information on permissions

